package junit5.stubbing;


import java.util.Map;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StubbingConsecutiveCallsTest {

    @Test
    public void subbingConsecutiveCalls() {
        Map mockMap = mock(Map.class);
        when(mockMap.get("some arg")).thenReturn(1).thenReturn(2).thenReturn(3);
        assertEquals(1, mockMap.get("some arg"));
        assertEquals(2, mockMap.get("some arg"));
        assertEquals(3, mockMap.get("some arg"));
    }

    @Test
    public void subbingConsecutiveCallsShortHand() {
        Map mockMap = mock(Map.class);
        when(mockMap.get("some arg")).thenReturn("one", "two", "three");
        assertEquals("one", mockMap.get("some arg"));
        assertEquals("two", mockMap.get("some arg"));
        assertEquals("three", mockMap.get("some arg"));
    }
}

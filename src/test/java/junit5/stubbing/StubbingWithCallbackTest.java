package junit5.stubbing;


import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class StubbingWithCallbackTest {

    @Test
    public void stubbingWithCallback() {
        Map mockMap = mock(Map.class);
        when(mockMap.get(anyString())).thenAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Object mock = invocation.getMock();
                return "Got Value : " + args[0];
            }
        });
        assertEquals("Got Value : ABC", mockMap.get("ABC"));
    }

    @Test
    public void stubbingWithCallbackWithDoAnswer() {
        Map mockMap = mock(Map.class);
        doAnswer(invocation -> Integer.MAX_VALUE).when(mockMap).get(anyInt());
        assertEquals(Integer.MAX_VALUE, mockMap.get(1));
    }
}

package junit5.stubbing;



import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class StubbingExceptionTest {

    @Test
    public void stubbingException() {
        List mockList = mock(List.class);
        when(mockList.add(anyInt())).thenThrow(new IllegalAccessError("Does not allow to add integer value to list"));
        mockList.add(1); // Should throw IllegalAccessError
    }

    @Test
    public void stubbingExceptionWithDoThrow() {
        Map mockMap = mock(Map.class);
        doThrow(NullPointerException.class).when(mockMap).get(anyString());
        mockMap.get("Should throw null pointer exception");
    }
}

package junit5.stubbing;



import java.util.List;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StubbingMethodCallTest {

    @Test
    public void stubbingCall() {
        List mockList = mock(List.class);
        when(mockList.get(anyInt())).thenReturn(Integer.MAX_VALUE);
        assertEquals(Integer.MAX_VALUE, mockList.get(1));
    }
}

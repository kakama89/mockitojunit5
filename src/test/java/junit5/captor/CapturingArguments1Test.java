package junit5.captor;


import junit5.entity.*;
import junit5.repository.UserRepositoryImpl;
import junit5.service.UserServiceImpl;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CapturingArguments1Test {
    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepositoryImpl userRepository;

    @Captor
    private ArgumentCaptor<User> captorUser;

    @Test
    public void testActiveUser() {
        when(userRepository.findById(anyInt())).thenCallRealMethod();
        userService.active(1);
        ArgumentCaptor<User> captorUser = ArgumentCaptor.forClass(User.class);
        verify(userRepository).update(captorUser.capture());
        User user = captorUser.getValue();
        assertEquals(UserStatus.ACTIVE, user.getStatus());
        assertNull(user.getInactiveDate());
    }
}

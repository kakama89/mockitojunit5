package junit5.mock;

import static org.mockito.Mockito.verify;

import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CreateMockWithAnnotation2Test {
    @Mock
    private Set mockSet;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createMockWithMockAnnotationAndMockitoJUnitRunner() {

        mockSet.add(1);
        mockSet.add("two");
        mockSet.clear();

        verify(mockSet).add(1);
        verify(mockSet).clear();
    }
}

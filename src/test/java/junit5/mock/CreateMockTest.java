package junit5.mock;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class CreateMockTest {

	@Test
    public void createMockWithMockMethod() {
        List mockList = mock(ArrayList.class);
        when(mockList.add(anyInt())).thenThrow(new IllegalAccessError("Does not allow to add integer value to list"));
        mockList.add(1);
    }
}

package junit5.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

//@RunWith(MockitoJUnitRunner.class) // for junit 4
@ExtendWith(MockitoExtension.class)
public class CreateMockWithAnnotation1Test {
    @Mock
    private List mockList;

    @Test
    public void createMockWithMockAnnotationAndMockitoJUnitRunner() {
        mockList.add("one");
        mockList.add("two");
        mockList.clear();

        verify(mockList).add("one");
        verify(mockList).clear();
        verify(mockList, times(2)).add(anyString());
    }
}

package junit5.mock;

import junit5.entity.User;
import junit5.entity.UserStatus;
import junit5.repository.UserRepositoryImpl;
import junit5.service.UserServiceImpl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.MessageFormat;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

//@RunWith(MockitoJUnitRunner.class) // for junit 4
@ExtendWith(MockitoExtension.class)
public class ResetMockTest {
    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepositoryImpl userRepository;

    @Captor
    private ArgumentCaptor<User> captorUser;
    
//	public ResetMockTest() {
//    	System.out.print(userRepository);// userRepository is null, that why you should init test fixture in the setUp method
//    }

    @BeforeEach
    public void setUp() {
        when(userRepository.findById(anyInt())).thenCallRealMethod(); // stubbing for each test method
    }

    @Test
    public void testActiveUserUserNotFoundShouldThrowException() {
        Mockito.reset(userRepository);  // we have add this line to reset mock object
        NullPointerException exception = assertThrows(NullPointerException.class, () -> userService.active(1));
        assertEquals(MessageFormat.format("User with id {0} not found", 1), exception.getMessage());
    }


    @Test
    public void testActiveUserSuccess() {
        userService.active(1);
        verify(userRepository).update(captorUser.capture());
        User user = captorUser.getValue();
        assertEquals(UserStatus.ACTIVE, user.getStatus());
        assertNull(user.getInactiveDate());
    }

}

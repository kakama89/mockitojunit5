package junit5.spy;


import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CreateSpyTest {
    @Spy
    private List listSpy;

    @Test
    public void createSpyWithStubbingAndVerifying() {
        when(listSpy.size()).thenReturn(1); // you can stub spy object
        assertEquals(1, listSpy.size());
        verify(listSpy).size(); // you can verify method call on spy object
    }

    @Test
    public void createSpyBySpyMethod() {
        Map<Integer, Integer> map = new HashMap<>();
        Map mapSpy = spy(map);
        //Impossible: real method is called so spy.get(0) throws IndexOutOfBoundsException (the list is yet empty)
        when(mapSpy.get(0)).thenReturn("foo");
        //You have to use doReturn() for stubbing
        doReturn("foo").when(mapSpy.get(0));
    }
}

package junit5.service;

import junit5.entity.User;
import junit5.entity.UserStatus;
import junit5.repository.UserRepository;

import java.text.MessageFormat;
import java.time.Instant;
import java.util.List;

public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void remove(Integer id) {
        userRepository.remove(id);
    }

    @Override
    public void active(Integer id) {
        User user = userRepository.findById(id);
        if (user == null) {
            throw new NullPointerException(MessageFormat.format("User with id {0} not found", id));
        }
        user.setStatus(UserStatus.ACTIVE);
        user.setInactiveDate(null);
        userRepository.update(user);
    }

    @Override
    public void inactive(Integer id) {
        User user = userRepository.findById(id);
        if (user == null) {
            throw new NullPointerException(MessageFormat.format("User with id {0} not found", id));
        }
        user.setStatus(UserStatus.INACTIVE);
        user.setInactiveDate(Instant.now());
        userRepository.update(user);
    }

    @Override
    public User add(String name, UserStatus status) {
        Integer maxUserId = findAll().stream().map(User::getId).reduce((a, b) -> a > b ? a : b).orElse(0);
        User user = new User(maxUserId + 1, name, status);
        userRepository.add(user);
        return user;
    }
}

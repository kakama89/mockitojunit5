package junit5.service;

import junit5.entity.User;
import junit5.entity.UserStatus;

import java.util.List;

public interface UserService {
    List<User> findAll();

    void remove(Integer id);

    void active(Integer id);

    void inactive(Integer id);

    User add(String name, UserStatus status);
}

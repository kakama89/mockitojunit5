package junit5.repository;

import junit5.entity.User;
import junit5.entity.UserStatus;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserRepositoryImpl implements UserRepository {
    public static Map<Integer, User> users = new HashMap<>();

    static {
        users.put(1, new User(1, "Jame", UserStatus.ACTIVE));
        users.put(2, new User(2, "Tracy", UserStatus.INACTIVE));
        users.put(3, new User(3, "Brian", UserStatus.ACTIVE));
    }

    @Override
    public List<User> findAll() {
        return users.values().stream().collect(Collectors.toList());
    }

    @Override
    public User findById(Integer id) {
        return users.get(id);
    }

    @Override
    public List<User> findByName(String name) {
        Stream<User> listUsers = users.values().stream();
        return listUsers.filter(u -> u.getName().equalsIgnoreCase(name)).collect(Collectors.toList());
    }

    @Override
    public void add(User user) {
        users.put(user.getId(), user);
    }

    @Override
    public void remove(Integer id) {
        users.remove(id);
    }

    @Override
    public void update(User user) {
        users.put(user.getId(), user);
    }
}

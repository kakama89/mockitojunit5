package junit5.repository;

import junit5.entity.User;

import java.util.List;

public interface UserRepository {
    List<User> findAll();

    User findById(Integer id);

    List<User> findByName(String name);

    void add(User user);

    void remove(Integer id);

    void update(User user);

}

package junit5.entity;

import java.text.MessageFormat;
import java.time.Instant;

public class User {

    private UserStatus status;

    public User(Integer id, String name, UserStatus status) {
        this.id = id;
        this.name = name;
        this.status = status;
        if (UserStatus.INACTIVE.equals(status)) {
            this.inactiveDate = Instant.now();
        }
    }

    private Integer id;

    private String name;

    private Instant inactiveDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public Instant getInactiveDate() {
        return inactiveDate;
    }

    public void setInactiveDate(Instant inactiveDate) {
        this.inactiveDate = inactiveDate;
    }

    @Override
    public String toString() {
        return MessageFormat.format("User[id :{0}, name:{1}, status :{2}, inactiveDate:{4}]]", id, name, status, inactiveDate);
    }
}

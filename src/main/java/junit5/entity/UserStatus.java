package junit5.entity;

public enum UserStatus {
    ACTIVE, INACTIVE
}
